# Deck of Cards #
Welcome! **The timer has now started, your start time and submission time will be noted.** Please take a few minutes to read the following instructions.

## Download ##
Please first download the project by clicking "Download repository" [here](https://bitbucket.org/abfitlon/deckofcards/downloads/).

## Objective ##
Write an object-oriented implementation of a standard deck of playing cards. A template project has been provided which you should modify however you see fit, with the exclusion of Program.cs (see rules below)

## Rules ##
1. The deck must contain all cards you would expect to find in a standard deck of 52 playing cards, excluding the joker cards.
    * You can find a reference of all cards [here](https://en.wikipedia.org/wiki/Standard_52-card_deck#Rank_and_color)
2. The program should output all cards to the console.
    * Program.cs provides some code which attempts to print each card to the console. Once your solution is complete, each card should be printed in the format ```<value> of <suit>```. For example, ```7 of Clubs``` or ```4 of Diamonds```
3. Program.cs should not be modified. It is possible to complete the task without modifying Program.cs at all, so please keep this in mind when writing your solution.

## Completion and Assessment ##
Once you have completed the task, please immediately email your solution as a zip to [oliver.foyle@abglobal.com](mailto:oliver.foyle@abglobal.com). Please **do not include the compiled exe or any binary/DLL files in the zip file as these will cause the email to be blocked**.

Good luck!