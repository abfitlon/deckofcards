using System;

namespace DeckOfCards
{
    class Program
    {
        static void Main()
        {
            // Initialise a full deck of playing cards
            var deck = new DeckOfCards();

            // Print every card to the console
            // Expected output example of one card: "7 of Clubs"
            foreach (var card in deck)
            {
                Console.WriteLine(card);
            }
            
            Console.ReadKey();
        }
    }
}
